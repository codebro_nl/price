var application = {
    /**
     * Build a chart
     * @param object selector
     * @param  string coin
     * @return string
     */
    getChart: function(selector, coin, time, orders) {
        var orderUrl = "";
        if (orders == true) {
            orderUrl = "/orders"
        }
        var request = $.ajax({
            "url": "load/"+time+"/"+coin+orderUrl,
            dataType: "json",
            success: function(request) {
                var timestamps = [];
                var responseSet = [];
                var index = 0;
                // Loop over all data from the Ajax request
                $.each(request, function(key, value) {
                    var requestData = [];
                    $.each(value.data, function() {
                        if ($.inArray(this.time, timestamps) === -1) {
                            timestamps.push(this.time);
                        }
                        requestData.push(this.volume);
                    });
                    responseSet[index] = {
                        label: key,
                        data: requestData,
                        borderColor: value.color,
                        backgroundColor: value.color,
                        fill: false
                    }
                    index++;
                });
                var chart = new Chart($(selector), {
                    type: "line",
                    data: {
                        labels: timestamps,
                        datasets: responseSet,
                    },
                    options: {
                        animation: {
                            duration: 0
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
        return this;
    }
};

$(document).ready(function() {

    /**
     * Execute functions
     */
    loadCharts();

    /**
     * Set an interval for refreshing data
     */
    setInterval(function() {
        loadCharts();
    }, 1000 * 60 * 5);

    /**
     * Load charts
     */
    function loadCharts() {
        $('div.chart-container').each(function() {
            var coin = $(this).data('coin');
            var time = "hour";
            var orders = false;
            if ($(this).data("type") == "orders") {
                orders = true;
            }
            var canvas = $("<canvas />")
                .addClass("chart")
                .prop("id", coin)
            ;
            $(this).html('');
            $(this).append(canvas);
            application.getChart(canvas, coin, time, orders);
        });
    }

    /**
     * Change the timeset of a chart
     */
    $(document).on("click", ".timeset > span", function() {
        $(this).parent().find("span").removeClass("badge-secondary").addClass("badge-light");
        $(this).removeClass("badge-light").addClass("badge-secondary");
        var container = $(this).closest('.panel-content').find(".chart-container");
        var coin = container.data("coin");
        var canvas = $("<canvas />")
            .addClass("chart")
            .prop("id", coin)
        ;
        var orders = false;
        if (container.data("type") == "orders") {
            orders = true;
        }
        container.html('');
        container.append(canvas);
        application.getChart(canvas, coin, $(this).data("timeset"), orders);
    });
});
