<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/', function(Request $request, Response $response, array $args) {
    return $this->view->render($response, 'dashboard.html.twig', [
        "name" => "PriceOfCrypto",
        "current" => "home",
    ]);
});

$app->get('/orders', function(Request $request, Response $response, array $args) {
    return $this->view->render($response, 'orders.html.twig', [
        "name" => "PriceOfCrypto",
        "current" => "orders",
    ]);
});

// Load coins
$app->get("/load/{time}/{coin}[/{orders}]", function(Request $request, Response $response, array $args) {
    if ($request->isXhr()) {
        $market = new \App\Controller\Bittrex();
        return $market->retrieve($args);
    } else {
        // otherwise we redirect back to the homepage
        return $response->withRedirect("/");
    }
});

// Imports
$app->get("/import[/{exchange}]", function(Request $request, Response $response, array $args) {
    // Should only be able to be executed by the machine his command line
    $load = new \App\Controller\Bittrex();
    return $load->import();
});

// Cronjobs
$app->get("/cron[/{action}]", function(Request $request, Response $response, array $args) {
    $action = null;
    if (isset($args["action"])) {
        $action = $args["action"];
    }
    // Should only be able to be executed by the machine his command line
    $schedular = new \App\Controller\Cronjob($action);
    return $schedular->run();
});

// Messenger Bot
$app->get("/bot", function(Request $request, Response $resonse, array $args) {
    $verifyToken = "mbot-poc";
    $accessToken = "EAAadkY4jfLcBAK82EiAoltrpVv2ylQaahWw2rOXQT7CRo4O1OVxIDPsJl2yN1ycD0A4DWZCZBhonAbVldfKkpRPh2dQ9bIe25ZBjSULLZBmdePpwMR7uon0tq3ZBc5x3tiBHqN3kgErZAWq13Mt9Ho9r3XY4WmDlryoqG3bO1o2wZDZD";

    // Verify the token when we are setting up
    if ($_REQUEST["hub_verify_token"] === $verifyToken) {
        echo $_REQUEST["hub_challenge"];
        die();
    }

    $input = json_decode(file_get_contents('php://input'), true);
    $senderId = $input['entry'][0]['messaging'][0]['sender']['id'];
    $messageText = $input['entry'][0]['messaging'][0]['message']['text'];

    $reply = "Sorry, ik begreep je niet. Probeer 'hi' eens.";
    if ($messageText == "hi") {
        $reply = "Yo";
    }

    $response = [
        "recipient" => [ "id" => $senderId ],
        "message" => [ "text" => $reply ],
    ];

    $ch = curl_init('https://graph.facebook.com/v2.10/me/messages?access_token='.$accessToken);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
});

https://graph.facebook.com/v2.6/me/subscribed_apps?access_token=EAAadkY4jfLcBAK82EiAoltrpVv2ylQaahWw2rOXQT7CRo4O1OVxIDPsJl2yN1ycD0A4DWZCZBhonAbVldfKkpRPh2dQ9bIe25ZBjSULLZBmdePpwMR7uon0tq3ZBc5x3tiBHqN3kgErZAWq13Mt9Ho9r3XY4WmDlryoqG3bO1o2wZDZD
