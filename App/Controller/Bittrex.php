<?php

namespace App\Controller;

use \App\Controller\IImport;
use \GuzzleHttp\Client;
use \App\Model\Crypto;
use \App\Model\CryptoDaily;
use \App\Model\CryptoHourly;

/**
 * Bittrex import description
 * @author Norman Reinhard <info@codebros.nl>
 */
class Bittrex implements IImport
{
    /**
     * Create an import
     */
    public function import()
    {
        $client = new Client();

        $request = $client->request("GET", "https://bittrex.com/api/v1.1/public/getmarketsummaries");
        $bittrex = json_decode($request->getBody());
        foreach ($bittrex->result as $values) {
            $nameSplit = explode("-", $values->MarketName);
            $data = new \stdClass();
            $data->from = $nameSplit[0];
            $data->to = $nameSplit[1];
            $data->high = $values->High;
            $data->low = $values->Low;
            $data->sell_orders = $values->OpenSellOrders;
            $data->buy_orders = $values->OpenBuyOrders;
            $this->store($data);
        }
    }

    /**
     * Retrieve data from the crypto table
     * @return array
     */
    public function retrieve($args)
    {
        $action = null;
        $orders = (isset($args["orders"])) ? true : false;
        switch ($args["time"]) {
            case "hour":
                if ($orders) {
                    $action = $this->grabOrdersHourly($args["coin"]);
                } else {
                    $action = $this->grabHourly($args["coin"]);
                }
                break;
            case "day":
                if ($orders) {
                    $action = $this->grabOrdersDay($args["coin"]);
                } else {
                    $action = $this->grabDay($args["coin"]);
                }
                break;
            case "week":
                if ($orders) {
                    $action = $this->grabOrdersWeek($args["coin"]);
                } else {
                    $action = $this->grabWeek($args["coin"]);
                }
                break;
        }
        return $action;
    }

    /**
     * Store data into crypto model
     * @param  stdClass $input
     * @return bool
     */
    public function store(\stdClass $input)
    {
        $crypto = new Crypto();
        $crypto->from = $input->from;
        $crypto->to = $input->to;
        $crypto->high = $input->high;
        $crypto->low = $input->low;
        $crypto->sell_orders = $input->sell_orders;
        $crypto->buy_orders = $input->buy_orders;
        $crypto->save();
        return true;
    }

    /**
     * Grab data by the hour
     * @param string $coin
     * @return stirng
     */
    private function grabHourly($coin)
    {
        // Retrieve data from the database
        $crypto = (new Crypto())
            ->where("to", $coin)
            ->whereIn("from", $this->getAllowedCoins())
            ->orderBy("created_at", "desc")
            ->take(12)
            ->get()
        ;

        $data = [];
        // insert into an array per result
        foreach ($crypto as $current) {
            if (!array_key_exists($current->from, $data)) {
                $data[$current->from] = [
                    "label" => $current->from,
                    "color" => $this->getCoinColor($current->from),
                    "data" => [],
                ];
            }
            $insert = [
                "time" => date('H:i', $current->created_at->getTimestamp()),
                "volume" => round(($current->high + $current->low) / 2, 2),
            ];
            array_unshift($data[$current->from]["data"], $insert);
        }
        return json_encode($data);
    }

    /**
     * Grab data by the hour
     * @param string $coin
     * @return stirng
     */
    private function grabOrdersHourly($coin)
    {
        // Retrieve data from the database
        $crypto = (new Crypto())
            ->where("to", $coin)
            ->whereIn("from", $this->getAllowedCoins())
            ->orderBy("created_at", "desc")
            ->take(12)
            ->get()
        ;

        $data = [
            "buy" => [
                "label" => "buy",
                "color" => "red",
                "data" => [],
            ],
            "sell" => [
                "label" => "sell",
                "color" => "green",
                "data" => [],
            ],
        ];
        // insert into an array per result
        foreach ($crypto as $current) {
            $insertBuy = [
                "time" => date('H:i', $current->created_at->getTimestamp()),
                "volume" => $current->buy_orders,
            ];
            array_unshift($data["buy"]["data"], $insertBuy);
            $insertSell = [
                "time" => date('H:i', $current->created_at->getTimestamp()),
                "volume" => $current->sell_orders,
            ];
            array_unshift($data["sell"]["data"], $insertSell);
        }
        return json_encode($data);
    }

    /**
     * Grab data by the day
     * @param string $coin
     * @return stirng
     */
    private function grabDay($coin)
    {
        // Retrieve data from the database
        $crypto = (new CryptoHourly())
            ->where("to", $coin)
            ->whereIn("from", $this->getAllowedCoins())
            ->orderBy("datetime", "desc")
            ->take(24)
            ->get()
        ;
        $data = [];
        // insert into an array per result
        foreach ($crypto as $current) {
            if (!array_key_exists($current->from, $data)) {
                $data[$current->from] = [
                    "label" => $current->from,
                    "color" => $this->getCoinColor($current->from),
                    "data" => [],
                ];
            }
            $insert = [
                "time" => date('H:i', strtotime($current->datetime)),
                "volume" => round(($current->high + $current->low) / 2, 2),
            ];
            array_unshift($data[$current->from]["data"], $insert);
        }
        return json_encode($data);
    }

    /**
     * Grab data by the hour
     * @param string $coin
     * @return stirng
     */
    private function grabOrdersDay($coin)
    {
        // Retrieve data from the database
        $crypto = (new CryptoHourly())
            ->where("to", $coin)
            ->whereIn("from", $this->getAllowedCoins())
            ->orderBy("datetime", "desc")
            ->take(24)
            ->get()
        ;

        $data = [
            "buy" => [
                "label" => "buy",
                "color" => "red",
                "data" => [],
            ],
            "sell" => [
                "label" => "sell",
                "color" => "green",
                "data" => [],
            ],
        ];
        // insert into an array per result
        foreach ($crypto as $current) {
            $insertBuy = [
                "time" => date('H:i', strtotime($current->datetime)),
                "volume" => $current->buy_orders,
            ];
            array_unshift($data["buy"]["data"], $insertBuy);
            $insertSell = [
                "time" => date('H:i', strtotime($current->datetime)),
                "volume" => $current->sell_orders,
            ];
            array_unshift($data["sell"]["data"], $insertSell);
        }
        return json_encode($data);
    }

    /**
     * Grab data by the day
     * @param string $coin
     * @return stirng
     */
    private function grabWeek($coin)
    {
        // Retrieve data from the database
        $crypto = (new CryptoDaily())
            ->where("to", $coin)
            ->whereIn("from", $this->getAllowedCoins())
            ->orderBy("datetime", "desc")
            ->take(7)
            ->get()
        ;
        $data = [];
        // insert into an array per result
        foreach ($crypto as $current) {
            if (!array_key_exists($current->from, $data)) {
                $data[$current->from] = [
                    "label" => $current->from,
                    "color" => $this->getCoinColor($current->from),
                    "data" => [],
                ];
            }
            $insert = [
                "time" => date('d M', strtotime($current->datetime)),
                "volume" => round(($current->high + $current->low) / 2, 2),
            ];
            array_unshift($data[$current->from]["data"], $insert);
        }
        return json_encode($data);
    }

    /**
     * Grab data by the hour
     * @param string $coin
     * @return stirng
     */
    private function grabOrdersWeek($coin)
    {
        // Retrieve data from the database
        $crypto = (new CryptoDaily())
            ->where("to", $coin)
            ->whereIn("from", $this->getAllowedCoins())
            ->orderBy("datetime", "desc")
            ->take(7)
            ->get()
        ;

        $data = [
            "buy" => [
                "label" => "buy",
                "color" => "red",
                "data" => [],
            ],
            "sell" => [
                "label" => "sell",
                "color" => "green",
                "data" => [],
            ],
        ];
        // insert into an array per result
        foreach ($crypto as $current) {
            $insertBuy = [
                "time" => date('d M', strtotime($current->datetime)),
                "volume" => $current->buy_orders,
            ];
            array_unshift($data["buy"]["data"], $insertBuy);
            $insertSell = [
                "time" => date('d M', strtotime($current->datetime)),
                "volume" => $current->sell_orders,
            ];
            array_unshift($data["sell"]["data"], $insertSell);
        }
        return json_encode($data);
    }

    /**
     * Return allowed coins in an array
     * @return array
     */
    private function getAllowedCoins()
    {
        return [
            "USDT",
            // "BTC",
            // "ETH",
        ];
    }

    /**
     * Get the corresponding color for a coin
     * @param string $coin
     * @return string
     */
    private function getCoinColor($coin) {
        $color = null;
        switch ($coin) {
            case 'BTC':
                $color = "#FF9900";
                break;
            case "ETH":
                $color = "grey";
                break;
            case "USDT":
                $color = "#85bb65";
                break;
        }
        return $color;
    }
}
