<?php

namespace App\Controller;

interface IImport
{
    public function import();
}
