<?php

namespace App\Controller;

use \App\Model\Crypto;
use \App\Model\CryptoHourly;
use \App\Model\CryptoDaily;

/**
 * Cronjob class
 *
 * @author Norman Reinhard <info@codebros.nl>
 * @since 1.01
 */
class Cronjob
{
    private $action;

    public function __construct($action = null)
    {
        $this->action = $action;
    }

    /**
     * Run several or more cronjobs
     * @version 1.01
     * @return bool
     */
    public function run()
    {
        switch ($this->action) {
            case 'hourly':
                $this->cleanUpCryptoHourly();
                break;
            case 'daily':
                $this->cleanUpCryptoDaily();
                break;
        }
        return true;
    }

    /**
     * Clean up all data from the crypto table
     * @return [type] [description]
     */
    private function cleanUpCryptoHourly()
    {
        $dateTime = (new \DateTime())
                ->modify("-1 hour")
        ;
        $crypto = (new Crypto())
                ->where("created_at", "<=", $dateTime->format('Y-m-d H:i:s'))
                ->orderBy("created_at", "DESC")
                ->get();

        $dataSet = [];
        // Gather all hours together per coin
        foreach ($crypto as $current) {
            $dateKey = date("Y-m-d H:00:00", $current->created_at->getTimestamp());
            $dataSet[$current->from . "-" . $current->to][$dateKey][] = [
                "from" => $current->from,
                "to" => $current->to,
                "high" => $current->high,
                "low" => $current->low,
                "buy_orders" => $current->buy_orders,
                "sell_orders" => $current->sell_orders,
            ];
            $deleteCrypto = Crypto::find($current->crypto_id);
            $deleteCrypto->delete();
        }

        // Now calculate the new averages and insert them in the database
        foreach ($dataSet as $market => $values) {
            foreach ($values as $time => $data) {
                $timestamp = date("Y-m-d H:i:s", strtotime($time));
                $divide = count($data);
                $high = 0;
                $low = 0;
                $buyOrders = 0;
                $sellOrders = 0;
                $from = "";
                $to = "";
                foreach ($data as $current) {
                    $from = $current["from"];
                    $to = $current["to"];
                    $high += $current["high"];
                    $low += $current["low"];
                    $buyOrders += $current["buy_orders"];
                    $sellOrders += $current["sell_orders"];
                }
                // insert into new table
                $hourly = new CryptoHourly();
                $hourly->from = $from;
                $hourly->to = $to;
                $hourly->high = $high / $divide;
                $hourly->low = $low / $divide;
                $hourly->buy_orders = $buyOrders / $divide;
                $hourly->sell_orders = $sellOrders / $divide;
                $hourly->datetime = $timestamp;
                $hourly->save();
            }
        }
    }

    /**
     * Clean up all data from the crypto table
     * @return [type] [description]
     */
    private function cleanUpCryptoDaily()
    {
        $dateTime = (new \DateTime())
                ->modify("-1 day")
        ;
        $crypto = (new CryptoHourly())
                ->where("datetime", ">=", $dateTime->format("Y-m-d 00:00:00"))
                ->where("datetime", "<=", $dateTime->format("Y-m-d 23:59:59"))
                ->orderBy("created_at", "DESC")
                ->get()
        ;
        $dataSet = [];
        // Gather all hours together per coin
        foreach ($crypto as $current) {
            $dateKey = date("Y-m-d", strtotime($current->datetime));
            $dataSet[$current->from . "-" . $current->to][$dateKey][] = [
                "from" => $current->from,
                "to" => $current->to,
                "high" => $current->high,
                "low" => $current->low,
                "buy_orders" => $current->buy_orders,
                "sell_orders" => $current->sell_orders,
            ];
            $deleteRecord = CryptoHourly::find($current->crypto_hourly_id);
            $deleteRecord->delete();
        }
        // Now calculate the new averages and insert them in the database
        foreach ($dataSet as $market => $values) {
            foreach ($values as $time => $data) {
                $timestamp = date("Y-m-d H:i:s", strtotime($time));
                $divide = count($data);
                $high = 0;
                $low = 0;
                $buyOrders = 0;
                $sellOrders = 0;
                $from = "";
                $to = "";
                foreach ($data as $current) {
                    $from = $current["from"];
                    $to = $current["to"];
                    $high += $current["high"];
                    $low += $current["low"];
                    $buyOrders += $current["buy_orders"];
                    $sellOrders += $current["sell_orders"];
                }
                // insert into new table
                $hourly = new CryptoDaily();
                $hourly->from = $from;
                $hourly->to = $to;
                $hourly->high = $high / $divide;
                $hourly->low = $low / $divide;
                $hourly->buy_orders = $buyOrders / $divide;
                $hourly->sell_orders = $sellOrders / $divide;
                $hourly->datetime = $timestamp;
                $hourly->save();
            }
        }
    }
}
