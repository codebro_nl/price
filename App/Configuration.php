<?php

namespace App;

/**
 * Configuration class
 * @author Norman Reinhard <info@codebros.nl>
 */
class Configuration
{
    private $db;

    /**
     * Check if environment is a localmachine or the server
     * @return boolean
     */
    private static function isDevelopment()
    {
        if (strpos($_SERVER["HTTP_HOST"], '.dev.local') !== false) {
            return true;
        }
        return false;
    }

    /**
     * Constructor of Configuration
     */
    public function __construct()
    {
        $this->db = new Database();
        $this->init();
    }

    /**
     * Initialise a database connection
     * @return null
     */
    private function init()
    {
        if (self::isDevelopment() === true) {
            $this->db->setDatabaseDriver("mysql");
            $this->db->setDatabaseHost("127.0.0.1");
            $this->db->setDatabaseName("crypto");
            $this->db->setDatabaseUser("root");
            $this->db->setDatabasePassword("root");
            $this->db->setDatabasePort(8889);
        } else {
            $this->db->setDatabaseDriver("mysql");
            $this->db->setDatabaseHost("127.0.0.1");
            $this->db->setDatabaseName("crypto");
            $this->db->setDatabaseUser("application");
            $this->db->setDatabasePassword("paarsekrokodil24");
            $this->db->setDatabasePort();
        }
        return $this->db->connect();
    }
}
