<?php

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * CryptoDaily model
 * @author Norman Reinhard <info@codebros.nl>
 */
class CryptoDaily extends Model
{
    protected $table = 'crypto_daily';

    protected $primaryKey = "crypto_daily_id";
}
