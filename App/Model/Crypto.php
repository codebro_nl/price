<?php

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * Crypto model
 * @author Norman Reinhard <info@codebros.nl>
 */
class Crypto extends Model
{
    protected $table = 'crypto';

    protected $primaryKey = "crypto_id";
}
