<?php

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * CryptoHourly model
 * @author Norman Reinhard <info@codebros.nl>
 */
class CryptoHourly extends Model
{
    protected $table = 'crypto_hourly';

    protected $primaryKey = "crypto_hourly_id";
}
