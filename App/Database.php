<?php

namespace App;

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Database class
 * @author Norman Reinhard <info@codebros.nl>
 */
class Database
{
    private $db_name;

    private $db_user;

    private $db_pass;

    private $db_host;

    private $db_port;

    private $db_driver;

    public function connect()
    {
        $capsule = new Capsule();
        $capsule->addConnection([
            'driver' => $this->getDatabaseDriver(),
            'host' => $this->getDatabaseHost(),
            'database' => $this->getDatabaseName(),
            'username' => $this->getDatabaseUser(),
            'password' => $this->getDatabasePassword(),
            'port' => $this->getDatabasePort(),
            'prefix' => '',
        ]);
        // Start Eloquent ORM
        $capsule->bootEloquent();
    }

    /**
     * Database name
     * @return string
     */
    public function getDatabaseName()
    {
        return $this->db_name;
    }

    /**
     * Set Database name
     * @return string
     */
    public function setDatabaseName($input)
    {
        $this->db_name = $input;
    }

    /**
     * Database username
     * @return string
     */
    public function getDatabaseUser()
    {
        return $this->db_user;
    }

    /**
     * set Database username
     * @return string
     */
    public function setDatabaseUser($input)
    {
        $this->db_user = $input;
    }

    /**
     * Database password
     * @return string
     */
    public function getDatabasePassword()
    {
        return $this->db_pass;
    }

    /**
     * set Database password
     * @return string
     */
    public function setDatabasePassword($input)
    {
        $this->db_pass = $input;
    }

    /**
     * Database host
     * @return string
     */
    public function getDatabaseHost()
    {
        return $this->db_host;
    }

    /**
     * set Database host
     * @return string
     */
    public function setDatabaseHost($input)
    {
        $this->db_host = $input;
    }

    /**
     * Database port
     * @return int
     */
    public function getDatabasePort()
    {
        return $this->db_port;
    }

    /**
     * Database port
     * @return int
     */
    public function setDatabasePort($input = null)
    {
        $this->db_port = $input;
    }

    /**
     * Database driver
     * @return string
     */
    public function getDatabaseDriver()
    {
        return $this->db_driver;
    }

    /**
     * Database driver
     * @return string
     */
    public function setDatabaseDriver($input)
    {
        $this->db_driver = $input;
    }
}
